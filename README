ALPM library overview & internals
=================================

Here is a list of the main objects and files from the ALPM (i.e. Arch Linux
Package Management) library. This document, whilst not exhaustive, also
indicates some limitations (on purpose, or sometimes due to its poor design) of
the library at the present time.

There is one special file,"alpm.h", which is the public interface that
should be distributed and installed on systems with the library. Only
structures, data and functions declared within this file are made available to
the frontend. Lots of structures are of an opaque type and their fields are
only accessible in read-only mode, through some clearly defined functions.

In addition to "alpm.h", the interfaces of "alpm_list.h" have also been made
available to the frontend. It is not a requirement for the frontend to use
these list functions; however, it prevents frontends from having to reimplement
a list data structure.

Several structures and functions have been renamed compared to pacman 2.9 code.
This was done at first for the sake of naming scheme consistency, and then
primarily because of potential namespace conflicts between library and frontend
spaces. Indeed, it is not possible to have two different functions with the
same name declared in both spaces. To avoid such conflicts, internal function
names have been prepended with "_alpm_".

In a general manner, public library functions are named "alpm_<type>_<action>"
(examples: alpm_trans_commit(), alpm_release(), alpm_pkg_getinfo(), ...).
Internal (and thus private) functions should be named "_alpm_XXX" for instance
(examples: _alpm_needbackup(), _alpm_runscriplet(), ...). Functions defined and
used inside a single file should be defined as "static".


[Initialization]

alpm_init() is used to initialize library internals and to create
a transparent handle object. Before its call, the library can't be used.

alpm_lib_release() just does the opposite (memory used by the library, and the
handle is freed).  After its call, the library is no longer available.


[Options]

In the future, the library will not use any configuration file.  It will be up
to the front end to The handle holds a
number of configuration options instead (IGNOREPKG, SYSLOG usage,
log file name, registered databases, ...).

All of the following options have a alpm_option_get_* and alpm_option_set_*
function for getting and setting the value.  The cannot be set before the
library is initialized.

* logcb: The callback function for "log" operations.
* dlcb: The callback function for download progress.
* logmask: The logging mask for which level of output is sent to the logcb.
* root: The root directory on which pacman operates (Default: /)
* dbpath: The base path to pacman's databases (Default: var/lib/pacman)
* cachedir: The base path to pacman's download cache (Default: var/cache/pacman)
* logfile: The base path to pacman's log file (Default: var/log/pacman.log)
* usesyslog: Log to syslog instead of `logfile` for file-base logging.
* upgradedelay: The time span to wait before listing a package as an upgrade (Default: 0)
* xfercommand: The command to use for downloading instead of pacman's internal
             downloading functionality.
* nopassiveftp: Do not use passive FTP commands for ftp connections.
* chomp: No way, easter eggs are secret!
* usecolor: Unimplemented, but for the future. You can assume what it means.

The following options also have a `alpm_option_add_*` function, as the values
are list structures (NOTE: The add functions are NOT plural, as they're in
english: alpm_option_get_noupgrades -> alpm_option_add_noupgrade).

* noupgrades: Files which will never be touched by pacman (extracted as .pacnew)
* noextracts: Files which will never be extracted at all (no .pacnew file)
* ignorepkgs: Packages to ignore when upgrading.
* holdpkgs: Packages which must be upgraded before continuing.

The following options are read-only, having ONLY alpm_option_get_* functions:

* localdb: A pmdb_t structure for the local (installed) database
* syncdbs: A list of pmdb_t structures to which pacman can sync from.


[Transactions]

The transaction sturcture permits easy manipulations of several packages
at a time (i.e. adding, upgrade and removal operations).

A transaction can be initiated with a type (ADD, UPGRADE or REMOVE),
and some flags (NODEPS, FORCE, CASCADE, ...).

Note: there can only be one type at a time: a transaction is either
created to add packages to the system, or either created to remove packages.
The frontend can't request for mixed operations: it has to run several
transactions, one at a time, in such a case.

The flags allow to tweak the library behaviour during its resolution.
Note, that some options of the handle can also modify the behavior of a
transaction (NOUPGRADE, IGNOREPKG, ...).

Note: once a transaction has been initiated, it is not possible anymore
to modify its type or its flags.

One can also add some targets to a transaction (alpm_trans_addtarget()).
These targets represent the list of packages to be handled.

Then, a transaction needs to be prepared (alpm_trans_prepare()). It
means that the various targets added, will be inspected and challenged
against the set of already installed packages (dependency checkings,

Last, a callback is associated with each transaction. During the
transaction resolution, each time a new step is started or done (i.e
dependency or conflict checking, package adding or removal, ...), the
callback is called, allowing the frontend to be aware of the progress of
the resolution. Can be useful to implement a progress bar.


[Package Cache]

libalpm maintains two caches for each DB.  One is a general package cache, the
other is a group cache (for package groups).  These caches are loaded on demand,
and freed when the libary is.
It is important to note tha, as a general rule, package structures should NOT be
freed manually, as they SHOULD be part of the cache.
The cache of a database is always updated by the library after
an operation changing the database content (adding and/or removal of
packages). Beware frontends ;)


[Package]

The package structure maintains all information for a package.  In general,
packages should never be freed from front-ends, as they should always be part of
the package cache.

The 'origin' data member indicates whether the package is from a file
(i.e. -U operations) or from the package cache.  In the case of a file, all data
members available are present in the structure.  Packages indicated as being
from the cache have data members filled on demand.  For this reason, the
alpm_pkg_get_* functions will load the data from the DB as needed.


[Errors]

The library provides a global variable pm_errno.
It aims at being to the library what errno is for C system calls.

Almost all public library functions are returning an integer value: 0
indicating success, -1 indicating a failure.
If -1 is returned, the variable pm_errno is set to a meaningful value
Wise frontends should always care for these returned values.

Note: the helper function alpm_strerror() can also be used to translate
the error code into a more friendly sentence.


[List - alpm_list_t] 
The alpm_list_t structure is a doubly-linked list for use with the libalpm
routines.  This type is provided publicly so that frontends are free to use it
if they have no native list type (C++, glib, python, etc all have list types).
See the proper man pages for alpm_list_t references.



PACMAN frontend overview & internals
====================================

Here are some words about the frontend responsibilities.
The library can operate only a small set of well defined operations and
dummy operations.

High level features are left to the frontend ;)

For instance, during a sysupgrade, the library returns the whole list of
packages to be upgraded, without any care for its content.
The frontend can inspect the list and perhaps notice that "pacman"
itself has to be upgraded. In such a case, the frontend can choose to
perform a special action.


[MAIN] (see pacman.c)

Calls for alpm_lib_init(), and alpm_lib_release().
Read the configuration file, and parse command line arguments.
Based on the action requested, it initiates the appropriate transactions
(see pacman_add(), pacman_remove(), pacman_sync() in files add.c,
remove.c and sync.c).


[CONFIGURATION] (see conf.c)

The frontend is using a configuration file, usually "/etc/pacman.conf".
Part of these options are only useful for the frontend only (mainly,
the download stuffs, and some options like HOLDPKG).
The rest is used to configure the library.


[ADD/UPGRADE/REMOVE/SYNC]

Nothing new here, excepted some reorganization.

The file pacman.c has been divided into several smaller files, namely
add.c, remove.c, sync.c and query.c, to hold the big parts: pacman_add,
pacman_remove, pacman_sync.

These 3 functions have been split to ease the code reading.


LIMITATIONS/BEHAVIOR CHANGES COMPARED TO PACMAN 2.9
===================================================

Excepted missing features still needing to be implemented, one can
notice the following limitations:

- If pacman is out of date, the frontend displays a warning and recommends
to give up the on-going transanction. The frontend does not allow to
upgrade pacman itself on-the-fly, and thus it should be restarted with
only "pacman" as a target.

- ...
