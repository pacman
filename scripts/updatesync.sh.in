#!/bin/bash
#
#   updatesync
#   @configure_input@
#
#   Copyright (c) 2004 by Jason Chu <jason@archlinux.org>
#   Derived from gensync (c) 2002-2006 Judd Vinet <jvinet@zeroflux.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#   USA.
#

# gettext initialization
export TEXTDOMAIN='pacman'
export TEXTDOMAINDIR='@localedir@'

myver='@PACKAGE_VERSION@'

# functions

usage() {
	printf "updatesync (pacman) %s\n\n" "$myver"
	printf "$(gettext "Usage: %s <action> <destfile> <option> [package_directory]")\n\n" "$0"
	printf "$(gettext "\
updatesync will update a sync database by reading a PKGBUILD and\n\
modifying the destfile. updatesync updates the database in a temporary\n\
directory and then compresses it to <destfile>.\n\n")"
	printf "$(gettext "There are two types of actions:\n\n")"
	printf "$(gettext "upd - Will update a package's entry or create it if it doesn't exist.\n      It takes the package's PKGBUILD as an option.\n")"
	printf "$(gettext "del - Will remove a package's entry from the db. It takes the package's\n      name as an option.\n")"
	echo
	printf "$(gettext "\
updatesync will calculate md5sums of packages in the same directory as\n\
<destfile>, unless an alternate [package_directory] is specified.\n\n")"
	echo "$(gettext "Example:  updatesync upd /home/mypkgs/custom.db.tar.gz PKGBUILD")"
	exit 0
}

version() {
	printf "updatesync (pacman) %s\n" "$myver"
	printf "$(gettext "\
Copyright (C) 2004 Jason Chu <jason@archlinux.org>.\n\n\
This is free software; see the source for copying conditions.\n\
There is NO WARRANTY, to the extent permitted by law.\n")"
}

error () {
	local mesg=$1; shift
	printf "==> ERROR: ${mesg}\n" "$@" >&2
}

die () {
	error $*
	exit 1
}

check_force () {
	local i
	for i in ${options[@]}; do
		local lc=$(echo $i | tr [:upper:] [:lower:])
		if [ "$lc" = "force" ]; then
			true
		fi
	done
	false
}

# PROGRAM START

if [ "$1" = "-h" -o "$1" = "--help" ]; then
	usage
	exit 0
fi

if [ "$1" = "-V" -o "$1" = "--version" ]; then
	version
	exit 0
fi

if [ $# -lt 3 ]; then
	usage
	exit 1
fi

# source system and user makepkg.conf
if [ -r @sysconfdir@/makepkg.conf ]; then
	source @sysconfdir@/makepkg.conf
else
	die "$(gettext "%s not found. Can not continue.")" "@sysconfdir@/makepkg.conf"
fi

if [ -r ~/.makepkg.conf ]; then
	source ~/.makepkg.conf
fi

if [ "$1" != "upd" -a "$1" != "del" ]; then
	usage
	exit 1
fi

action=$1
pkgdb=$2
option=$3
pkgdir="$(pwd)"
if [ "$4" != "" ]; then
	pkgdir="$4"
fi
opt_force=""

if [ ! -f "$option" ]; then
	die "$(gettext "%s not found")" $option
fi

if [ "$action" = "upd" ]; then # INSERT / UPDATE
	unset pkgname pkgver pkgrel options

	source $option || die "$(gettext "failed to parse %s")" $option
	if [ "$arch" = 'any' ]; then
		CARCH='any'
	fi
	pkgfile="$pkgdir/$pkgname-$pkgver-$pkgrel-$CARCH.$PKGEXT"

	if [ ! -f "$pkgfile" ]; then
		die "$(gettext "could not find %s-%s-%s-%s.%s - aborting")" $pkgname $pkgver $pkgrel $CARCH $PKGEXT
	fi

	if check_force; then
		opt_force="--force"
	fi

	repo-add "$pkgdb" $opt_force "$pkgfile"
else # DELETE
	fname="$(basename $option)"
	if [ "$fname" = "PKGBUILD" ]; then
		unset pkgname pkgver pkgrel options
		source $option
	else
		pkgname=$1
	fi

	repo-remove "$pkgdb" "$pkgname"
fi

exit 0
# vim: set ts=2 sw=2 noet:
