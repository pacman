/////
vim:set ts=4 sw=4 syntax=asciidoc noet:
/////
See the Arch Linux website at http://www.archlinux.org[] for more current
information on the distribution and the pacman family of tools, and
http://wiki.archlinux.org/index.php/Arch_Packaging_Standards[] for
recommendations on packaging standards.


Bugs
----
Bugs? You must be kidding, there are no bugs in this software. But if we
happen to be wrong, send us an email with as much detail as possible to
mailto:pacman-dev@archlinux.org[].


Authors
-------
* Judd Vinet <jvinet@zeroflux.org>
* Aurelien Foret <aurelien@archlinux.org>
* Aaron Griffin <aaron@archlinux.org>
* Dan McGee <dan@archlinux.org>

See the 'AUTHORS' file for additional contributors.
