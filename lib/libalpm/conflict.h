/*
 *  conflict.h
 *
 *  Copyright (c) 2002-2006 by Judd Vinet <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 *  USA.
 */
#ifndef _ALPM_CONFLICT_H
#define _ALPM_CONFLICT_H

#include "alpm.h"
#include "db.h"
#include "package.h"

#define CONFLICT_FILE_LEN 512

struct __pmconflict_t {
	char package1[PKG_NAME_LEN];
	char package2[PKG_NAME_LEN];
};

struct __pmfileconflict_t {
	char target[PKG_NAME_LEN];
	pmfileconflicttype_t type;
	char file[CONFLICT_FILE_LEN];
	char ctarget[PKG_NAME_LEN];
};

pmconflict_t *_alpm_conflict_new(const char *package1, const char *package2);
int _alpm_conflict_isin(pmconflict_t *needle, alpm_list_t *haystack);
alpm_list_t *_alpm_innerconflicts(alpm_list_t *packages);
alpm_list_t *_alpm_outerconflicts(pmdb_t *db, alpm_list_t *packages);
alpm_list_t *_alpm_checkconflicts(pmdb_t *db, alpm_list_t *packages);
alpm_list_t *_alpm_db_find_fileconflicts(pmdb_t *db, pmtrans_t *trans, char *root);

#endif /* _ALPM_CONFLICT_H */

/* vim: set ts=2 sw=2 noet: */
